#!/usr/bin/env python

import re
import subprocess


class LineType(object):

    def __init__(self, name, pattern):
        self.name = name
        self.pattern = re.compile(pattern)


class Node(object):

    def __init__(self, spaces):
        self.spaces = spaces
        self.data = dict()


# GETTERS
def get_info():
    # EXECUTE SUBPROCESS
    output = execute(['pactl', 'info'])

    # RETURN
    return to_map(output)


def get_cards():
    # EXECUTE SUBPROCESS
    output = execute(['pactl', 'list', 'cards'])

    # RETURN
    return to_map(output)


def get_sinks():
    # EXECUTE SUBPROCESS
    output = execute(['pactl', 'list', 'sinks'])

    # RETURN
    return to_map(output)


def get_sources():
    # EXECUTE SUBPROCESS
    output = execute(['pactl', 'list', 'sources'])

    # RETURN
    return to_map(output)


def get_sink_inputs():
    # EXECUTE SUBPROCESS
    output = execute(['pactl', 'list', 'sink-inputs'])

    # RETURN
    return to_map(output)


def get_source_outputs():
    # EXECUTE SUBPROCESS
    output = execute(['pactl', 'list', 'source-outputs'])

    # RETURN
    return to_map(output)


# UTILS
line_types = [
    LineType('object', '^(?:Card|Source|Sink|Sink Input|Source Output)\s#([0-9]+)$'),
    LineType('colon', '^(.*)\:\s(.*)$'),
    LineType('equals', '^(.*)\s=\s(.*)$'),
    LineType('node', '^(.*):$'),
]


def to_map(output):
    # BUILD DICTIONARY
    nodes = [Node(-1)]

    for line in output:
        space_count = count_leading_char(line, ' ')
        tab_count = count_leading_char(line, '\t')
        space_count += tab_count * 8

        # FIGURE OUT WHAT TYPE OF LINE IT IS
        line = line.strip()

        matcher = None
        line_type = None

        for line_type in line_types:
            matcher = line_type.pattern.match(line)

            if matcher is not None:
                break

        # GET THE PARENT
        parent = None
        failed_i = -1

        for i in range(len(nodes)):
            _parent = nodes[i]

            if _parent.spaces < space_count:
                parent = _parent
            else:
                failed_i = i

        # REMOVE LOWER LEVELS
        if failed_i > -1:
            nodes = nodes[:i + 1]

        # WORK ON THE LINE TYPE
        if matcher is not None:
            node = Node(space_count)

            if line_type.name == 'object':
                # UNPACK
                number = matcher.group(1)

                # INIT NODE
                node.data['__id__'] = number

                # BIND NODE DATA
                parent.data[number] = node.data

                # print(space_count, parent.spaces, matcher.group(1), line)
            elif line_type.name in ['colon', 'equals']:
                # UNPACK
                if line_type.name == 'colon':
                    assign_index = line.index(': ')
                    assign_length = 2
                else:
                    assign_index = line.index(' = ')
                    assign_length = 3

                key = line[:assign_index]
                value = line[assign_index + assign_length:]

                # STRIP THE VALUE
                value = value.strip('"')

                # INIT NODE
                node.data['__value__'] = value

                # BIND NODE DATA
                parent.data[key] = node.data

                # print(space_count, key, value, line)
            elif line_type.name == 'node':
                # UNPACK
                key = matcher.group(1)

                # BIND NODE DATA
                parent.data[key] = node.data

                # print(space_count, parent.spaces, key)
            else:
                raise ValueError('Unknown type.')

            # APPEND NODE
            nodes.append(node)

    # RETURN
    return nodes[0].data


def count_leading_char(line, char):
    spaces = 0

    for c in line:
        if c == char:
            spaces += 1
        else:
            break

    return spaces


def execute(cmd):
    p = subprocess.Popen(cmd,
                         stdout=subprocess.PIPE,
                         universal_newlines=True)

    stdout, stderr = p.communicate()

    if p.returncode:
        raise subprocess.CalledProcessError(p.returncode, cmd)

    return stdout.split('\n')
