#!/usr/bin/env python
import subprocess

import pactl.pactl as pactl


def main():
    stage = 0

    while stage >= 0:
        if stage == 0:
            options = ['output', 'input']
            output_input = dmenu('output\ninput')

            if output_input not in options:
                stage = -1
                continue
            else:
                stage = 1
                continue
        else:
            # GET INFO
            info = pactl.get_info()
            cards = pactl.get_cards()

            # FIND DEFAULT
            if output_input == 'output':
                default_s_name = info['Default Sink']['__value__']
                ss = pactl.get_sinks()
            else:
                default_s_name = info['Default Source']['__value__']
                ss = pactl.get_sources()

            default_s = get_sink_source_by_name(ss, default_s_name)

            if default_s is None:
                raise ValueError('Couldn\'t find default.')

            # QUERY PORT TO BE USED
            port_list = list()
            port_dmenu_list = ''

            ports = get_ports(cards, output_input)

            for port in ports:
                port_list.append(port['name'])
                port_dmenu_list += port['name'] + '\n'

            selected_port_name = dmenu(port_dmenu_list)

            if selected_port_name not in port_list:
                stage = 0
                continue

            selected_port = None

            for port in ports:
                if selected_port_name == port['name']:
                    selected_port = port
                    break

            if selected_port is None:
                raise ValueError('selected port doesn\'t exist.')

            # COMPARE CURRENT PROFILE TO CURRENT CARD PROFILE
            card_id = selected_port['card_id']
            card_profile = selected_port['card_profile']

            satisfied = False

            for profile in selected_port['profiles']:
                if profile == card_profile:
                    satisfied = True
                    break

            if satisfied:
                print('Current card profile satisfies the requested port.')
            else:
                ports = card_profile.split('+')

                # FIND WHICH OTHER PORTS AREN'T THE SAME TYPE WE ARE LOOKING FOR
                # AND ADD THEM TO A LIST, SO WE CAN FIND A PROFILE THAT HAS
                # THOSE AS WELL
                keep_ports = []

                for _port in ports:
                    if output_input not in _port:
                        keep_ports.append(_port)

                # FIND A PROFILE THAT SATISFIES PREVIOUS PORTS AND THE NEW ONE
                profile = None

                for _profile in selected_port['profiles']:
                    satisfies_all = True

                    for _port in keep_ports:
                        if _port not in _profile:
                            satisfies_all = False
                            break

                    if satisfies_all:
                        profile = _profile

                if profile is None:
                    raise ValueError('A card profile that satisfies all ports couldn\'t be found.')

                print('Card "' + selected_port['card_name'] + '" profile will be changed to: ' + profile)

                # CHANGE PROFILE
                execute([
                    'pactl',
                    'set-card-profile',
                    selected_port['card_name'],
                    profile
                ])

            # UPDATE SINK/SOURCES
            # AFTER PROFILE IS UPDATED SINK/SOURCES CHANGE
            if output_input == 'output':
                ss = pactl.get_sinks()
            else:
                ss = pactl.get_sources()

            # MATCH PORT TO SINK/SOURCE
            selected_s = get_sink_source_by_port(ss, selected_port)

            if selected_s is None:
                raise ValueError('Couldn\'t match port to sink/source.')

            # GET I/O APPLICATIONS
            io_maps = None

            if output_input == 'output':
                io_maps = pactl.get_sink_inputs()
            else:
                io_maps = pactl.get_source_outputs()

            # NOTE: WE ARE ALMOST DONE, WE JUST NEED TO UPDATE THE IO SINK/SOURCES BY MOVING THEM TO THE RIGHT ONE
            # WE ALSO HAVE TO UPDATE THE DEFAULT SINK/SOURCE
            update_io_id = list()

            for io_id, io in io_maps.items():
                io_s = io['Sink' if output_input == 'output' else 'Source']['__value__']

                if io_s != selected_s['__id__']:
                    execute([
                        'pactl',
                        'move-sink-input' if output_input == 'output' else 'move-source-output',
                        io_id,
                        selected_s['Name']['__value__']
                    ])

            # SET DEFAULT SINK/SOURCE
            execute([
                'pactl',
                'set-default-sink' if output_input == 'output' else 'set-default-source',
                selected_s['Name']['__value__']
            ])

            # UPDATE STAGE
            stage = -1


def execute(cmd):
    p = subprocess.Popen(cmd,
                         stdout=subprocess.PIPE,
                         universal_newlines=True)

    stdout, stderr = p.communicate()

    if p.returncode:
        raise subprocess.CalledProcessError(p.returncode, cmd)

    return stdout.split('\n')


def dmenu(options):
    p = subprocess.Popen(['dmenu'],
                         stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE,
                         universal_newlines=True)

    stdout, stderr = p.communicate(input=options)

    if p.returncode != 0:
        return None

    return stdout.strip()


def get_ports(cards, input_output):
    available_ports = list()

    for card_id, card in cards.items():
        ports = card['Ports']

        card_name = card['Name']['__value__']
        card_alias = card['Properties']['alsa.card_name']['__value__']
        card_profile = card['Active Profile']['__value__']

        for port_id, port in ports.items():
            if input_output not in port_id:
                continue

            if ', not available)' in port['__value__']:
                continue

            name = port['__value__']
            pidx = name.index(' (priority')
            name = name[:pidx]

            profiles = port['Part of profile(s)']['__value__']
            profiles = profiles.split(", ")

            available_ports.append({
                'id': port_id,
                'name': card_alias + ' - ' + name,
                'card_id': card_id,
                'card_name': card_name,
                'card_alias': card_alias,
                'card_profile': card_profile,
                'profiles': profiles
            })

    return available_ports


def get_sink_source_by_name(sink_source_dict, name):
    matched = None

    for s_id, s in sink_source_dict.items():
        if s['Name']['__value__'] == name:
            matched = s
            break

    return matched


def get_sink_source_by_port(sink_source_dict, port):
    matched = None

    for s_id, s in sink_source_dict.items():
        if 'Ports' not in s:
            continue

        ports = s['Ports']

        for port_id in ports:
            if port_id == port['id']:
                matched = s
                break

    return matched
