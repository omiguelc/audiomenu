# LIBRARIES
import os
import sys
from glob import glob

if __name__ == '__main__':
    # ADD PROJECT TO PATH
    project_path = os.path.dirname(os.path.realpath(__file__))
    sys.path.append(project_path + '/src')

    # PROJECT SPECIFIC MODULES
    import audiomenu.menu as menu

    # RUN MAIN
    menu.main()